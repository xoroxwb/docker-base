ARG IMAGE_BASE=alpine
# hadolint ignore=DL3006
FROM ${IMAGE_BASE}

# Copy root filesystem
COPY rootfs /

# Install base system
RUN \
    echo '@edge http://dl-cdn.alpinelinux.org/alpine/edge/main' >> /etc/apk/repositories && \
    echo '@edge http://dl-cdn.alpinelinux.org/alpine/edge/community' >> /etc/apk/repositories && \
    echo '@edge http://dl-cdn.alpinelinux.org/alpine/edge/testing' >> /etc/apk/repositories

# Entrypoint & CMD
ENTRYPOINT ["/entrypoint"]

# Build arguments
ARG BUILD_DATE
ARG IMAGE_NAME
ARG IMAGE_DESC
ARG IMAGE_VERS
ARG IMAGE_ARCH
ARG IMAGE_TYPE
ARG IMAGE_ARCH
ARG IMAGE_REF
ARG IMAGE_URL

# Labels
LABEL \
    io.image.name="${IMAGE_NAME}" \
    io.image.description="${IMAGE_DESC}" \
    io.image.arch="${IMAGE_ARCH}" \
    io.image.type="${IMAGE_TYPE}" \
    io.image.version="${IMAGE_VERS}" \
    maintainer="Daniel Müller <xoroxwb@gitlab.com>" \
    org.opencontainers.image.title="${IMAGE_NAME}" \
    org.opencontainers.image.description="${IMAGE_DESC}" \
    org.opencontainers.image.vendor="xoroxwb" \
    org.opencontainers.image.authors="Daniel Müller <xoroxwb@gitlab.com>" \
    org.opencontainers.image.licenses="MIT" \
    org.opencontainers.image.url="https://hub.docker.com/repository/docker/xoroxwb/${IMAGE_NAME}" \
    org.opencontainers.image.source="${IMAGE_URL}" \
    org.opencontainers.image.documentation="${IMAGE_URL}/blob/master/README.md" \
    org.opencontainers.image.created="${BUILD_DATE}" \
    org.opencontainers.image.revision="${IMAGE_REF}" \
    org.opencontainers.image.version="${IMAGE_VERS}"
